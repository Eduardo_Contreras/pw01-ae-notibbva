DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AgregaNoticia`(
IN `pTitulo` varchar(45),
IN `pDescripcionC` text,
IN `pNoticia` text,
IN `pCategoria` int,
IN `pImagen1` varchar(100),
IN `pImagen2` varchar(100),
IN `pVideo` varchar(100),
IN `pUsuario` varchar(30))
BEGIN

	DECLARE idUsuario INT DEFAULT 0;
    
    SELECT Cve_Usuario
    INTO idUsuario
    FROM usuario
    WHERE Usuario = pUsuario;

	INSERT INTO `dbnotibbva`.`noticia`
	(`Titulo`,
	`DescripcionC`,
	`Noticia`,
	`Categoria`,
	`Imagen1`,
	`Imagen2`,
	`Video`,
	`Usuario`)
	VALUES
	(`pTitulo`,
	`pDescripcionC`,
    `pNoticia`,
    `pCategoria`,
    `pImagen1`,
    `pImagen2`,
    `pVideo`,
    idUsuario);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `BuscaNoticia`(
IN `pTitulo` varchar(45))
BEGIN
SELECT N.Titulo,
    N.DescripcionC,
    N.Noticia,
    N.Categoria,
    C.Descripción,
    N.Imagen1,
    N.Imagen2,
    N.Video,
    N.Estado,
    N.Usuario,
    U.Usuario, 
    N.Reacciones
FROM noticia N
INNER JOIN usuario AS U
ON U.Cve_Usuario = N.Usuario
INNER JOIN categoria AS C
ON C.Cve_Categoria = N.categoria
WHERE N.Titulo = pTitulo;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetCategory`()
BEGIN
	SELECT `categoria`.`Cve_Categoria`,
		`categoria`.`Descripción`
	FROM `dbnotibbva`.`categoria`
    ORDER BY `categoria`.`Descripción`;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetComentario`(
IN `pTitulo` varchar(45)
)
BEGIN
	SELECT u.Usuario, c.Descripcion
	FROM comentario c
	INNER JOIN usuario AS u
	ON u.Cve_Usuario = c.Cve_Us
	INNER JOIN noticia AS n
	ON n.Cve_Noticia = c.Cve_Not
	WHERE n.Titulo = pTitulo;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetNoticiaR`()
BEGIN
SELECT N.Titulo,
    N.DescripcionC,
    N.Noticia,
    N.Categoria,
    C.Descripción,
    N.Imagen1,
    N.Imagen2,
    N.Video,
    N.Estado,
    N.Usuario,
    U.Usuario, 
    N.Reacciones
FROM noticia N
INNER JOIN usuario AS U
ON U.Cve_Usuario = N.Usuario
INNER JOIN categoria AS C
ON C.Cve_Categoria = N.categoria
WHERE Estado = 'No Aprobado';
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetNoticias`()
BEGIN
SELECT N.Titulo,
    N.DescripcionC,
    N.Noticia,
    N.Categoria,
    C.Descripción,
    N.Imagen1,
    N.Imagen2,
    N.Video,
    N.Estado,
    N.Usuario,
    U.Usuario, 
    N.Reacciones
FROM noticia N
INNER JOIN usuario AS U
ON U.Cve_Usuario = N.Usuario
INNER JOIN categoria AS C
ON C.Cve_Categoria = N.categoria
WHERE Estado = 'Aprobado';
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetNoticiasG`(
IN pUsuario VARCHAR(45))
BEGIN

DECLARE idUs INT DEFAULT 0;

SELECT Cve_Usuario
INTO idUs
FROM usuario
WHERE Usuario = pUsuario;


SELECT N.Titulo,
    N.DescripcionC,
    N.Noticia,
    N.Categoria,
    C.Descripción,
    N.Imagen1,
    N.Imagen2,
    N.Video,
    N.Estado,
    N.Usuario,
    U.Usuario, 
    N.Reacciones
FROM noticia N
INNER JOIN usuario AS U
ON U.Cve_Usuario = N.Usuario
INNER JOIN categoria AS C
ON C.Cve_Categoria = N.categoria
INNER JOIN usuarioguardanoticia AS UN
ON UN.Cve_Not = N.Cve_Noticia
WHERE UN.Cve_Us = idUs;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetNoticiasN`(
IN `pTitulo` varchar(45))
BEGIN
SELECT N.Titulo,
    N.DescripcionC,
    N.Noticia,
    N.Categoria,
    C.Descripción,
    N.Imagen1,
    N.Imagen2,
    N.Video,
    N.Estado,
    N.Usuario,
    U.Usuario, 
    N.Reacciones
FROM noticia N
INNER JOIN usuario AS U
ON U.Cve_Usuario = N.Usuario
INNER JOIN categoria AS C
ON C.Cve_Categoria = N.categoria
WHERE N.Titulo = pTitulo;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetNoticiasP`()
BEGIN
SELECT N.Titulo,
    N.DescripcionC,
    N.Noticia,
    N.Categoria,
    C.Descripción,
    N.Imagen1,
    N.Imagen2,
    N.Video,
    N.Estado,
    N.Usuario,
    U.Usuario, 
    N.Reacciones
FROM noticia N
INNER JOIN usuario AS U
ON U.Cve_Usuario = N.Usuario
INNER JOIN categoria AS C
ON C.Cve_Categoria = N.categoria
WHERE Estado = 'En espera';
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GuardarNoticia`(
IN pUsuario VARCHAR(45),
IN pTitulo VARCHAR(45))
BEGIN

DECLARE idUs INT DEFAULT 0;
DECLARE idNot INT DEFAULT 0;

SELECT Cve_Usuario
INTO idUs
FROM usuario
WHERE Usuario = pUsuario;

SELECT Cve_Noticia
INTO idNot
FROM noticia
WHERE Titulo = pTitulo;

	INSERT INTO `dbnotibbva`.`usuarioguardanoticia`
	(`Cve_Us`,
	`Cve_Not`)
	VALUES
	(idUs,
	idNot);
    
    
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUs`(
IN `pUsuario` varchar(25),
IN `pContraseña` varchar(30),
IN `pCorreo` varchar(50),
IN `pImage` varchar(200))
BEGIN
	INSERT INTO usuario
(Usuario,
Contraseña,
Correo,
Image)
VALUES
(pUsuario,
pContraseña,
pCorreo,
pImage);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `LogInAdminUser`(
IN `pUsuario` varchar(25),
IN `pContraseña` varchar(30),
IN `pCorreo` varchar(50),
IN `pImage` varchar(200),
IN `pRol` varchar(20))
BEGIN
INSERT INTO usuario
(Usuario,
Contraseña,
Correo,
Image,
Rol)
VALUES
(pUsuario,
pContraseña,
pCorreo,
pImage,
pRol);
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `LogInUser`(
IN `pUsuario` varchar(25),
IN `pContraseña` varchar(30))
BEGIN
	SELECT
    u.Usuario,
    u.Rol,
    u.Image
	FROM usuario u
	WHERE u.Usuario = pUsuario
	AND u.Contraseña = pContraseña;
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModAprNoticia`(
IN `pTitulo` varchar(45),
IN `pEstado` varchar(45),
IN `pDescripcion` varchar(100),
IN `pUsuario` varchar(30))
BEGIN

	DECLARE ID_US INT DEFAULT 0;
    DECLARE ID_NOT INT DEFAULT 0;
    
    SELECT Cve_Usuario
    INTO ID_US
    FROM usuario
    where Usuario = pUsuario;
    
    SELECT Cve_Noticia
    INTO ID_NOT
    FROM noticia
    where Titulo = pTitulo;
    
    UPDATE `dbnotibbva`.`noticia`
	SET
	`Estado` = pEstado
	WHERE `Titulo` = pTitulo;
    
    IF pEstado = 'No Aprobado' THEN
    INSERT INTO `dbnotibbva`.`comentario`
	(`Descripcion`,
	`Cve_Us`,
	`Cve_Not`,
	`Tipo`)
	VALUES
	(pDescripcion,
    ID_US,
    ID_NOT,
    'UNN');
	END IF;
END$$
DELIMITER ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ModificarUs`(
IN `pUsuario` varchar(25),
IN `pContraseña` varchar(30),
IN `pCorreo` varchar(50),
IN `pImage` varchar(200),
IN `Condicion` varchar(3))
BEGIN

IF Condicion = 'PEI' THEN
UPDATE usuario
SET
Contraseña = pContraseña,
Correo = pCorreo,
Image = pImage
WHERE Usuario = pUsuario;

ELSEIF Condicion = 'PE' THEN
UPDATE usuario
SET
Contraseña = pContraseña,
Correo = pCorreo
WHERE Usuario = pUsuario;

ELSEIF Condicion = 'PI' THEN
UPDATE usuario
SET
Contraseña = pContraseña,
Image = pImage
WHERE Usuario = pUsuario;

ELSEIF Condicion = 'EI' THEN
UPDATE usuario
SET
Correo = pCorreo,
Image = pImage
WHERE Usuario = pUsuario;

ELSEIF Condicion = 'P' THEN
UPDATE usuario
SET
Contraseña = pContraseña
WHERE Usuario = pUsuario;

ELSEIF Condicion = 'E' THEN
UPDATE usuario
SET
Correo = pCorreo
WHERE Usuario = pUsuario;

ELSEIF Condicion = 'I' THEN
UPDATE usuario
SET
Image = pImage
WHERE Usuario = pUsuario;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateNoticia`(
IN `pTitulo` varchar(45),
IN `pDescripcionC` text,
IN `pNoticia` text,
IN `pCategoria` int,
IN `pImagen1` varchar(100),
IN `pImagen2` varchar(100),
IN `pVideo` varchar(100))
BEGIN

UPDATE `dbnotibbva`.`noticia`
SET
`DescripcionC` =pDescripcionC,
`Noticia` =pNoticia,
`Categoria` = pCategoria,
`Imagen1` = pImagen1,
`Imagen2` = pImagen2,
`Video` = pVideo,
`Estado` = 'En espera'
WHERE `Titulo` = pTitulo;

END$$
DELIMITER ;

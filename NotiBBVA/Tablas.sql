CREATE TABLE `categoria` (
  `Cve_Categoria` int NOT NULL AUTO_INCREMENT,
  `Descripción` varchar(45) NOT NULL,
  PRIMARY KEY (`Cve_Categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `comentario` (
  `Cve_Comentario` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Cve_Us` int unsigned NOT NULL,
  `Reacciones` int DEFAULT '0',
  `Cve_Not` int NOT NULL,
  `Tipo` varchar(10) NOT NULL,
  PRIMARY KEY (`Cve_Comentario`),
  KEY `Fk_Usuario_idx` (`Cve_Us`),
  KEY `Fk_Not_idx` (`Cve_Not`),
  CONSTRAINT `Fk_Not` FOREIGN KEY (`Cve_Not`) REFERENCES `noticia` (`Cve_Noticia`),
  CONSTRAINT `Fk_Us` FOREIGN KEY (`Cve_Us`) REFERENCES `usuario` (`Cve_Usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `noticia` (
  `Cve_Noticia` int NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(45) NOT NULL,
  `DescripcionC` text NOT NULL,
  `Noticia` text NOT NULL,
  `Categoria` int NOT NULL,
  `Imagen1` varchar(100) DEFAULT NULL,
  `Imagen2` varchar(100) DEFAULT NULL,
  `Video` varchar(100) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT 'En espera',
  `Usuario` int unsigned NOT NULL,
  `Reacciones` int DEFAULT '0',
  PRIMARY KEY (`Cve_Noticia`),
  KEY `Fk_Categoria_idx` (`Categoria`),
  KEY `Fk_Usuario_idx` (`Usuario`),
  CONSTRAINT `Fk_Categoria` FOREIGN KEY (`Categoria`) REFERENCES `categoria` (`Cve_Categoria`),
  CONSTRAINT `Fk_Usuario` FOREIGN KEY (`Usuario`) REFERENCES `usuario` (`Cve_Usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `usuario` (
  `Cve_Usuario` int unsigned NOT NULL AUTO_INCREMENT,
  `Usuario` varchar(25) DEFAULT NULL,
  `Contraseña` varchar(30) NOT NULL,
  `Rol` varchar(5) DEFAULT 'UN',
  `Correo` varchar(50) NOT NULL,
  `Image` varchar(200) DEFAULT 'assets/images/Imagen1606842170316.png',
  PRIMARY KEY (`Cve_Usuario`),
  UNIQUE KEY `Cve_Usuario_UNIQUE` (`Cve_Usuario`),
  UNIQUE KEY `Usuario_UNIQUE` (`Usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `usuarioguardanoticia` (
  `Cve_Us` int unsigned NOT NULL,
  `Cve_Not` int NOT NULL,
  KEY `FkUs_idx` (`Cve_Us`),
  KEY `Fk_Not_idx` (`Cve_Not`),
  CONSTRAINT `Fk_Noti` FOREIGN KEY (`Cve_Not`) REFERENCES `noticia` (`Cve_Noticia`),
  CONSTRAINT `Fk_Usu` FOREIGN KEY (`Cve_Us`) REFERENCES `usuario` (`Cve_Usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;




	
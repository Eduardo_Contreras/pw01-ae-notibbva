<%-- 
    Document   : NewNoticia
    Created on : 6/12/2020, 12:44:05 AM
    Author     : eduar
--%>

<%@page import="com.mycompany.notibbva.models.Categorias"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Categorias> cate = (List<Categorias>)request.getAttribute("Cat");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="styles/main.css" rel="stylesheet" type="text/css"/>
        <link href="styles/Inicio.css" rel="stylesheet" type="text/css"/>
        <link href="styles/AddNews.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
        <title>Agregar Noticia</title>
    </head>
    <body>
        <% if(session.getAttribute("Usuario") == null)
                {
        %>
        <script languaje="Javascript">
        alert("Su sesión se ha cerrado, favor de iniciar sesión nuevamente");
        <%
                }
        %>
        </script>
        <div class = "encabezado">
            <div id = "titulo">
                <img src="assets/images/Logo NOTIBBVA.png" alt="" width="200" height="100"/>
            </div>
            <div id ="barrabuscador">
                <input type="text" id="buscador">
                <button id = "BotonBuscar">Buscar</button>
            </div>
            <div id ="InfoUsuario">
                <h2 class ="nombreGlobal"><%= session.getAttribute("Usuario")%></h2>
                <ul class ="nv">
                    <li><img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="60" height="60"/>
                    <ul>
                        <form action="IndexController" method="GET">
                        <input type ="submit" class ="botonCerrar"  value ="Cerrar Sesión">
                        </form>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="FormNoticia">
            <br><h2 class="titulopag">Agregar Noticia</h2>
            <form class ="noticia" enctype="multipart/form-data" action="AddNewController" method="POST" autocomplete="off">
                <br><input type ="text" name ="UsuarioE" value="<%= session.getAttribute("Usuario")%>" readonly><p>
                <br><input type ="text" name ="Titulo" placeholder="Titulo de Noticia" required><p>
                <br><textarea name ="DescC" id="Desc" placeholder="Descripción Corta" required></textarea><p>
                <br><textarea name ="DescN" id="Not" placeholder="Noticia" required></textarea><p>
                <br><select name ="Categoria">
                    <option value="-1">Categoria</option>
                    <%
                        for(Categorias cat : cate){
                    %>
                        <option value="<%= cat.getId()%>"><%=cat.getNombre()%></option>
                    <%
                        }
                    %>
                </select>
                <br><h2 class="titulopag">Multimedia - Imagen</h2>
                <div id ="mostrarimagen">
                </div>
                        <input type ="file" id="archivoInput" name ="Imagen" onchange = "return validarExt(500,250,0)">
                <div id ="mostrarimagen2">
                </div>
                        <input type ="file" id="archivoInput2" name ="Imagen2" onchange = "return validarExt2(500,250,0)">
                        <br><h2 class="titulopag">Multimedia - Video</h2>
                        <input type ="file" id="archivoInput3" name ="Video">
                        <input type ="submit" class ="botonANoticia"  value ="Cargar Noticia"> 
            </form>
        </div>
    </body>
</html>

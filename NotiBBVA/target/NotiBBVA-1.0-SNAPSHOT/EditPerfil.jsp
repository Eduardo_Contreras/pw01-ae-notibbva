<%-- 
    Document   : EditPerfil
    Created on : 30/11/2020, 03:04:28 AM
    Author     : eduar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    session = request.getSession();
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Editar Perfil</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="styles/main.css" rel="stylesheet" type="text/css"/>
        <link href="styles/Inicio.css" rel="stylesheet" type="text/css"/>
        <link href="styles/EditPerfil.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
    </head>
    <body>
       <div class = "encabezado">
            <div id = "titulo">
                <img src="assets/images/Logo NOTIBBVA.png" alt="" width="200" height="100"/>
            </div>
            <div id ="barrabuscador">
                <input type="text" id="buscador">
                <button id = "BotonBuscar">Buscar</button>
            </div>
            <div id ="InfoUsuario">
                <h2 class ="nombreGlobal"><%= session.getAttribute("Usuario")%></h2>
                <ul class ="nv">
                    <li><img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="60" height="60"/>
                    <ul>
                        <li><a href="index.jsp">Cerrar Sesión</a></li>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class ="Conteiner">
            <h1 id="tituloSec">Editar Perfil</h1>
             <form class ="inicio" enctype="multipart/form-data" action="ModController" method="POST" autocomplete="off">
            <div class = "Imagen">
                <br>
                Imagen Actual: 
                <BR>
                <div id ="mostrarimagen">
                    <img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="200" height="200" style="border-radius: 100px"/>
                </div>
                <br>
                ¿Cambiar Imagen?<br>
                <input type ="file" id="archivoInput" name ="ImagenM" onchange = "return validarExt(200,200,100)">
            </div>
            <div class ="edit">
                <br><br><br>
                Usuario:
                <br><input type ="text" name ="UsuarioM" value="<%= session.getAttribute("Usuario")%>" readonly><p>
                Cambiar contraseña: 
                <br><input type ="password" name ="ContraM" pattern="[A-Za-z0-9.]{8,15}"><p>
                Cambiar correo: 
                <br><input type ="email" name ="CorreoM"><p>
            </div>
                <input type ="submit" class ="botonModificar"  value ="Guardar Cambios">   
             </form>
        </div>
    </body>
</html>

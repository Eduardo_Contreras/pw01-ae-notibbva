<%-- 
    Document   : EditarNoticia
    Created on : 8/12/2020, 11:27:19 AM
    Author     : eduar
--%>

<%@page import="com.mycompany.notibbva.models.Comentarios"%>
<%@page import="com.mycompany.notibbva.models.Noticia"%>
<%@page import="com.mycompany.notibbva.models.Categorias"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    session = request.getSession();
    List<Categorias> cate = (List<Categorias>)request.getAttribute("Cat");
    Noticia not = (Noticia)request.getAttribute("nNewN");
    List<Comentarios> com = (List<Comentarios>)request.getAttribute("Com");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="styles/main.css" rel="stylesheet" type="text/css"/>
        <link href="styles/Inicio.css" rel="stylesheet" type="text/css"/>
        <link href="styles/AddNews.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
        <title>Editar Noticia</title>
    </head>
    <body>
        <div class = "encabezado">
            <div id = "titulo">
                <img src="assets/images/Logo NOTIBBVA.png" alt="" width="200" height="100"/>
            </div>
            <div id ="barrabuscador">
                <input type="text" id="buscador">
                <button id = "BotonBuscar">Buscar</button>
            </div>
            <div id ="InfoUsuario">
                <h2 class ="nombreGlobal"><%= session.getAttribute("Usuario")%></h2>
                <ul class ="nv">
                    <li><img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="60" height="60"/>
                    <ul>
                        <form action="IndexController" method="GET">
                        <input type ="submit" class ="botonCerrar"  value ="Cerrar Sesi�n">
                        </form>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="FormNoticia">
            <br><h2 class="titulopag">Editar Noticia</h2>
            <form class ="noticia" enctype="multipart/form-data" action="UpdateNew" method="POST" autocomplete="off">
                <br><input type ="text" name ="UsuarioE" value="<%= session.getAttribute("Usuario")%>" readonly><p>
                <br><input type ="text" name ="Titulo" value="<%= not.getTitulo() %>" required><p>
                <br><textarea name ="DescC" id="Desc" required ><%= not.getDescripcionC()%></textarea><p>
                <br><textarea name ="DescN" id="Not"  required><%= not.getNoticia()%></textarea><p>
                <br><select name ="Categoria">
                    <option value="-1">Categoria</option>
                    <%
                        for(Categorias cat : cate){
                    %>
                        <option value="<%= cat.getId()%>"><%=cat.getNombre()%></option>
                    <%
                        }
                    %>
                </select>
                <br><h2 class="titulopag">Multimedia - Imagen</h2>
                <div id ="mostrarimagen">
                    <img src= "<%= not.getImagen1() %>" alt=""  width="500" height="250"/>
                </div>
                        <input type ="file" id="archivoInput" name ="Imagen" onchange = "return validarExt(500,250,0)">
                <div id ="mostrarimagen2">
                    <img src= "<%= not.getImagen2() %>" alt=""  width="500" height="250"/>
                </div>
                        <input type ="file" id="archivoInput2" name ="Imagen2" onchange = "return validarExt2(500,250,0)">
                        <br><h2 class="titulopag">Multimedia - Video</h2>
                        <input type ="file" id="archivoInput3" name ="Video">
                        <input type ="submit" class ="botonANoticia"  value ="Enviar Noticia"> 
            </form>
                <br><h2 class="titulopag">Comentarios</h2>
                <%
                    for(Comentarios c : com){
                %>
                <div class ="comentarios"> 
                    <h2 id="UsCom"><%= c.getUsuario() %></h2>
                    <h4 id="comen"><%= c.getDescripcion() %></h4>
                </div>
                <%
                    }
                %>
        </div>          
    </body>
</html>

<%@page import="com.mycompany.notibbva.models.Noticia"%>
<%@page import="com.mycompany.notibbva.models.Categorias"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Categorias> cate = (List<Categorias>)request.getAttribute("Cat");
    Noticia not = (Noticia)request.getAttribute("nNewN");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="styles/main.css" rel="stylesheet" type="text/css"/>
        <link href="styles/Inicio.css" rel="stylesheet" type="text/css"/>
        <link href="styles/AddNews.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
        <title>Agregar Noticia</title>
    </head>
    <body>
        <% if(session.getAttribute("Usuario") == null)
                {
        %>
        <script languaje="Javascript">
        alert("Su sesión se ha cerrado, favor de iniciar sesión nuevamente");
        <%
                }
        %>
        </script>
        <div class = "encabezado">
            <div id = "titulo">
                <img src="assets/images/Logo NOTIBBVA.png" alt="" width="200" height="100"/>
            </div>
            <div id ="barrabuscador">
                <input type="text" id="buscador">
                <button id = "BotonBuscar">Buscar</button>
            </div>
            <div id ="InfoUsuario">
                <h2 class ="nombreGlobal"><%= session.getAttribute("Usuario")%></h2>
                <ul class ="nv">
                    <li><img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="60" height="60"/>
                    <ul>
                        <form action="IndexController" method="GET">
                        <input type ="submit" class ="botonCerrar"  value ="Cerrar Sesión">
                        </form>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="FormNoticia">
            <br><h2 class="titulopag">Revisar Noticia</h2>
            <form class ="noticia" action="EstadoNotController" method="POST" autocomplete="off">
                <br><input type ="text" name ="UsuarioE" value="<%= not.getNameUs() %>" readonly><p>
                <br><input type ="text" name ="Titulo" value="<%= not.getTitulo() %>" readonly><p>
                <br><textarea name ="DescC" id="Desc"  readonly><%= not.getDescripcionC()%></textarea><p>
                <br><textarea name ="DescN" id="Not"  readonly><%= not.getNoticia() %></textarea><p>
                <br><select name ="Categoria">
                    <option value="<%= not.getIdCateg()%>"><%= not.getNameCate()%></option>
                </select>
                <br><h2 class="titulopag">Multimedia - Imagen</h2>
                <%
                    if("".equals(not.getImagen1())){
                %>
                    <h4 class="titulopag">No se agrego Imagen</h4>
                <%
                    }
                    else{
                %>
                <div id ="mostrarimagen">
                    <img src= "<%= not.getImagen1() %>" alt=""  width="500" height="250"/>
                </div>
                <%
                    }
                %>
                
                 <%
                    if("".equals(not.getImagen2())){
                %>
                <h4 class="titulopag">No se agrego Imagen</h4>
                <%
                    }
                    else{
                %>
                <div id ="mostrarimagen2">
                    <img src= "<%= not.getImagen2() %>" alt=""  width="500" height="250"/>
                </div>
                <%
                    }
                %>
                        <br><h2 class="titulopag">Multimedia - Video</h2>
                        <%
                            if("".equals(not.getVideo()))
                            {
                        %>
                        <h4 class="titulopag">No se agrego video</h4>
                        <%
                            }
                            else
                            {
                        %>
                            Video: not.getVideo()
                        <%
                            }
                        %>
                        <br><select name ="Estado">
                                    <option value="-1">Selecciona un estado</option>
                                    <option value="0">No Aprobado</option>
                                    <option value="1">Aprobado</option>
                                </select>
                        <br><br><input type ="text" name ="UsuarioC" value="<%= session.getAttribute("Usuario") %>" readonly>
                        <br><br><input type="text" name="motivo" placeholder="Motivo de rechazo">
                        <input type ="submit" class ="botonANoticia"  value ="Enviar"> 
            </form>
        </div>
    </body>
</html>

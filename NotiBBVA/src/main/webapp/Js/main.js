/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function validarExt(w, h, b){
    var archivoInput = document.getElementById('archivoInput');
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.jpg|.png)$/i;
    
    if(!extPermitidas.exec(archivoRuta))
    {
        alert('Asegurate de elegir un formato de imagen valido');
        archivoInput.value = "";
        return false;
    }
    else{
        if(archivoInput.files && archivoInput.files[0])
        {
            var visor = new FileReader();
            visor.onload = function(e)
            {
                document.getElementById('mostrarimagen').innerHTML = 
                        '<embed src = "'+e.target.result+'" width = "'+w+'"px height = "'+h+'"px style = "border-radius: '+b+'px";>';
            };
            visor.readAsDataURL(archivoInput.files[0]); 
        }
    }
    
}

function validarExt2(w, h, b){
    var archivoInput = document.getElementById('archivoInput2');
    var archivoRuta = archivoInput.value;
    var extPermitidas = /(.jpg|.png)$/i;
    
    if(!extPermitidas.exec(archivoRuta))
    {
        alert('Asegurate de elegir un formato de imagen valido');
        archivoInput.value = "";
        return false;
    }
    else{
        if(archivoInput.files && archivoInput.files[0])
        {
            var visor = new FileReader();
            visor.onload = function(e)
            {
                document.getElementById('mostrarimagen2').innerHTML = 
                        '<embed src = "'+e.target.result+'" width = "'+w+'"px height = "'+h+'"px style = "border-radius: '+b+'px";>';
            };
            visor.readAsDataURL(archivoInput.files[0]); 
        }
    }
    
}

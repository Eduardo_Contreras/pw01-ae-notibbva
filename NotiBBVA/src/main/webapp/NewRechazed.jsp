<%@page import="java.util.List"%>
<%@page import="com.mycompany.notibbva.models.Noticia"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    session = request.getSession();
    List<Noticia> noti = (List<Noticia>)request.getAttribute("nNewP");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
         <link href="styles/Inicio.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
        <title>Revisar Noticia</title>
    </head>
    <body>
        <div class = "encabezado">
            <div id = "titulo">
                <img src="assets/images/Logo NOTIBBVA.png" alt="" width="200" height="100"/>
            </div>
            <div id ="barrabuscador">
                <input type="text" id="buscador">
                <button id = "BotonBuscarM">Buscar</button>
            </div>
            <div id ="InfoUsuario">
                <h2 class ="nombreGlobal"><%= session.getAttribute("Usuario")%></h2>
                <ul class ="nv">
                    <li><img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="60" height="60"/>
                    <ul>
                        <li><a href="EditPerfil.jsp">Editar Perfil</a></li>
                        
                        <%if(!"UN".equals(session.getAttribute("Rol"))){
                            if("UA".equals(session.getAttribute("Rol"))){%>
                        <li><a href="AgregarUs.jsp">Agregar Usuario</a></li>
                        <%
                            }
                        %>
                        <li>
                            <form action="CategoryController" method="GET">
                            <input type ="submit" class ="botonAgregarNoticia"  value ="Agregar Noticia">
                            </form>
                        </li>
                        <%
                            }
                        %>
                        <li>
                            <form action="IndexController" method="GET">
                            <input type ="submit" class ="botonCerrar"  value ="Cerrar Sesi�n">
                            </form>
                        </li>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="contenedor">
            <h2 style="margin-left: 70px">Noticias Rechazadas</h2><br>
            <%
                for(Noticia element : noti){
            %>
            <div class="card mb-3 col-12" style="max-width: 540px;">
             <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="<%= element.getImagen1() %>" height="150px" style="margin-top: 25px" class="card-img" alt="...">
                </div>
             <div class="col-md-8">
                <div class="card-body">
                    <form action="EditNewController" method="POST">
                        <input type ="text" name="titulo" value="<%=element.getTitulo() %>" readonly style="border: none; font-weight: bold;">
                    <p class="card-text"><%= element.getDescripcionC() %></p>
                    <p class="card-text"><small class="text-muted">Categoria. <%= element.getNameCate() %></small></p>
                    <input type ="submit" class ="botonSeleccionaNoticia"  value ="Click para ver">
                    </form>
                </div>
              </div>
            </div>
          </div>
            <%
                }
            %>
        </div>
        </div>
    </body>
</html>


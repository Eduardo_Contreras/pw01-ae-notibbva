<%-- 
    Document   : AgregarUs
    Created on : 1/12/2020, 11:07:59 AM
    Author     : eduar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="styles/Inicio.css" rel="stylesheet" type="text/css"/>
         <link href="styles/SignInAdmi.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
        <title>Agregar Usuario</title>
    </head>
    <body>
        <div class = "encabezado">
            <div id = "titulo">
                <img src="assets/images/Logo NOTIBBVA.png" alt="" width="200" height="100"/>
            </div>
            <div id ="barrabuscador">
                <input type="text" id="buscador">
                <button id = "BotonBuscar">Buscar</button>
            </div>
            <div id ="InfoUsuario">
                <h2 class ="nombreGlobal"><%= session.getAttribute("Usuario")%></h2>
                <ul class ="nv">
                    <li><img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="60" height="60"/>
                    <ul>
                        <li>
                            <form action="LogOfController" method="POST">
                            <input type ="submit" class ="botonCerrar"  value ="Cerrar Sesión">
                            </form>
                        </li>
                        <li><a href="EditPerfil.jsp">Editar Perfil</a></li>
                        <li><a href="EditPerfil.jsp">Noticia</a></li>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <br>
        <div class="cont">
            <h2 class="tit">Agregar Usuario</h2>
            <br>
            <form class="reg" enctype="multipart/form-data" action="SignInAdmController" method="POST" autocomplete="off">
                <div class="datos">
                <p>
                <h3>Usuario: </h3>
                    <input type ="text" name ="UsuarioA" minlength = "3" required>
                </p>
                <p>
                <h3> Contraseña:</h3>
                    <input type ="password" name ="ContraA" pattern="[A-Za-z0-9]{8,15}" required>
                </p>
                <p>
                <h3>Correo:</h3> <input type ="email" name ="CorreoA">
                </p>
                <p>
                <h3>Rol: </h3>
                <select name="RolA">
                    <option value="Def">Seleccione una opción</option>
                    <option value="UCC">Creador de Contenido</option>
                    <option value="UM">Moderador</option>
                    <option value="UE">Editor</option>
                </select>
                </p>
                </div>
                <div class ="im">
                <p>
                <h3> Imagen: </h3> 
                    <BR>
                    <div id ="mostrarimagen">
                      
                    </div>
                        <input type ="file" id="archivoInput" name ="Imagen" onchange = "return validarExt(200,200,100)">
                </div>
                <input type ="submit" class ="botonRegistrar"  value ="Registrar">
            </form>
        </div>
    </body>
</html>

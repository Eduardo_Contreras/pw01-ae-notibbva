<%-- 
    Document   : Login
    Created on : 30/11/2020, 02:54:26 AM
    Author     : eduar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Pantalla inicio de sesión</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="styles/login.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="fondo"></div>
            <div class="Registro" id="login">
            <br/><h1 class = "titulo">Inicio de Sesión</h1>
            <form class ="inicio" action="LogInController" method="POST" autocomplete="off">
                <p>
                    Usuario: <input type ="text" name ="UsuarioL" minlength = "3" required>
                </p>
                <p>
                    Contraseña: <input type ="password" name ="ContraL" pattern="[[A-Za-z0-9.]{8,15}" required>
                </p>
                <p></p>      
                    <input type ="submit" class ="botonLogin"  value ="Iniciar">
            </form>
            </div>
        <div class="Registro" id="registro">
            <br/><h1 class = "titulo">Registro</h1>
            <form class ="inicio" enctype="multipart/form-data" action="SignInController" method="POST" autocomplete="off">
                <p>
                    Usuario: <input type ="text" name ="UsuarioR" minlength = "3" required>
                </p>
                <p>
                    Contraseña: <input type ="password" name ="ContraR" pattern="[A-Za-z0-9.]{8,15}" required>
                </p>
                <p>
                    Correo: <input type ="email" name ="CorreoR" required>
                </p>
                <p>
                    Imagen: 
                    <BR>
                    <div id ="mostrarimagen">
                      
                    </div>
                    <input type ="file" id="archivoInput" name ="Imagen" onchange = "return validarExt(100,100,50)" required>
                    <input type ="submit" class ="botonRegistrar"  value ="Registrar">
            </form>
        </div>
    </body>
</html>


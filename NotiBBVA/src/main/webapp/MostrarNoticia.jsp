<%@page import="com.mycompany.notibbva.models.Noticia"%>
<%@page import="com.mycompany.notibbva.models.Categorias"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List<Categorias> cate = (List<Categorias>)request.getAttribute("Cat");
    Noticia not = (Noticia)request.getAttribute("nNewN");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="styles/main.css" rel="stylesheet" type="text/css"/>
        <link href="styles/Inicio.css" rel="stylesheet" type="text/css"/>
        <link href="styles/AddNews.css" rel="stylesheet" type="text/css"/>
        <script src="Js/main.js" type="text/javascript"></script>
        <title>Agregar Noticia</title>
    </head>
    <body>
        </script>
        <div class = "encabezado">
            <div id = "titulo">
                <img src="assets/images/Logo NOTIBBVA.png" alt="" width="200" height="100"/>
            </div>
            <div id ="barrabuscador">
                <input type="text" id="buscador">
                <button id = "BotonBuscar">Buscar</button>
            </div>
            <div id ="InfoUsuario">
                <% if(session.getAttribute("Usuario") != null)
                {
                %>
                <h2 class ="nombreGlobal"><%= session.getAttribute("Usuario")%></h2>
                <%
                    }else{
                %>
                <h2 class ="nombreGlobal">Anonimo</h2>
                <%
                    }
                %>
                <ul class ="nv">
                    <%if(session.getAttribute("Usuario") != null)
                     {
                    %>
                    <li><img src= "<%= session.getAttribute("Imagen")%>" alt=""  width="60" height="60"/>
                     <%
                         }else{
                    %>
                    <li><img src="assets/images/Default Picture Profile.png" alt=""  width="60" height="60"/>
                        <%
                            }
                        %>
                    <ul>
                        <form action="IndexController" method="GET">
                        <input type ="submit" class ="botonCerrar"  value ="Cerrar Sesión">
                        </form>
                    </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="FormNoticia">
            <br><h2 class="titulopag">Ver Noticia</h2>
            <form class ="noticia" action="UsuarioGNoticiaController" method="POST" autocomplete="off">
                <br><input type ="text" name ="UsuarioE" value="<%= not.getNameUs() %>" readonly><p>
                <br><input type ="text" name ="Titulo" value="<%= not.getTitulo() %>" readonly><p>
                <br><textarea name ="DescC" id="Desc"  readonly><%= not.getDescripcionC()%></textarea><p>
                <br><textarea name ="DescN" id="Not"  readonly><%= not.getNoticia() %></textarea><p>
                <br><select name ="Categoria">
                    <option value="<%= not.getIdCateg()%>"><%= not.getNameCate()%></option>
                </select>
                <br><h2 class="titulopag">Multimedia - Imagen</h2>
                <%
                    if("".equals(not.getImagen1())){
                %>
                <%
                    }
                    else{
                %>
                <div id ="mostrarimagen">
                    <img src= "<%= not.getImagen1() %>" alt=""  width="500" height="250"/>
                </div>
                <%
                    }
                %>
                
                 <%
                    if("".equals(not.getImagen2())){
                %>
                <%
                    }
                    else{
                %>
                <div id ="mostrarimagen2">
                    <img src= "<%= not.getImagen2() %>" alt=""  width="500" height="250"/>
                </div>
                <%
                    }
                %>
                        <br><h2 class="titulopag">Multimedia - Video</h2>
                        <%
                            if("".equals(not.getVideo()))
                            {
                        %>
                        <%
                            }
                            else
                            {
                        %>
                            Video: not.getVideo()
                        <%
                            }
                        %>
                        <% if(session.getAttribute("Usuario") != null){ %>
                        <input type="hidden" name ="UsuarioA" value="<%= session.getAttribute("Usuario") %>" readonly>
                        <input type ="submit" class ="botonGNoticia"  value ="Agregar a favoritos"> 
                        <%}%>
            </form>
        </div>
    </body>
</html>


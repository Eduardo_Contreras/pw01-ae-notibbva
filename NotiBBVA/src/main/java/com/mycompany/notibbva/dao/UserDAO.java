/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.dao;

import com.mycompany.notibbva.models.User;
import com.mycompany.notibbva.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author eduar
 */
public class UserDAO {
    
//CREATE TABLE `prueba`.`new_table` (
// `idUser` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//  `User` VARCHAR(45) NULL,
//  `Password` VARCHAR(45) NULL,
//  `Email` VARCHAR(100) NULL,
//  PRIMARY KEY (`idUser`),
//  UNIQUE INDEX `idUser_UNIQUE` (`idUser` ASC) VISIBLE,
//  UNIQUE INDEX `User_UNIQUE` (`User` ASC) VISIBLE,
//  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC) VISIBLE);
    
    public static int insertUser(User us)
    {
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call insertUs(?, ?, ?, ?)";
            CallableStatement statement = con.prepareCall(sql);  
            statement.setString(1, us.getUser());
            statement.setString(2, us.getPass());
            statement.setString(3, us.getEmail());
            statement.setString(4, us.getPathimage());
            return statement.executeUpdate();
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public static int LogInAdminUser(User us2)
    {
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call LogInAdminUser(?, ?, ?, ?, ?)";
            CallableStatement statement = con.prepareCall(sql);  
            statement.setString(1, us2.getUser());
            statement.setString(2, us2.getPass());
            statement.setString(3, us2.getEmail());
            statement.setString(4, us2.getPathimage());
            statement.setString(5, us2.getRol());
            return statement.executeUpdate();
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public static User LogInUser(User usLog)
    {
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call LogInUser(?, ?)";
            CallableStatement statement = con.prepareCall(sql);  
            statement.setString(1, usLog.getUser());
            statement.setString(2, usLog.getPass());
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                String user = resultSet.getString(1);
                String rol = resultSet.getString(2);
                String pathImage = resultSet.getString(3);
                return new User(user, rol, pathImage);
            }
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
     public static int ModUser(User us, String Mod)
    {
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call ModificarUs(?, ?, ?, ?, ?)";
            CallableStatement statement = con.prepareCall(sql);  
            statement.setString(1, us.getUser());
            statement.setString(2, us.getPass());
            statement.setString(3, us.getEmail());
            statement.setString(4, us.getPathimage());
            statement.setString(5, Mod);
            return statement.executeUpdate();
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
}

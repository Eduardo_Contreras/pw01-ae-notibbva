/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.dao;

import com.mycompany.notibbva.models.UsuarioNoticia;
import com.mycompany.notibbva.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author eduar
 */
public class UsuarioNoticiaDAO {
    public static int guardanoticia(UsuarioNoticia UN){
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call GuardarNoticia(?, ?)";
            CallableStatement statement = con.prepareCall(sql);  
            statement.setString(1, UN.getUsName());
            statement.setString(2, UN.getNotName());
            return statement.executeUpdate();
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.dao;

import com.mycompany.notibbva.models.Categorias;
import com.mycompany.notibbva.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

public class CategoriaDAO {
    
    public static List<Categorias> getCategorias() throws SQLException{
        List<Categorias> categorias = new ArrayList<>();
        Connection con = null;
        try{
            con = DbConnection.getConnection();  
            String sql = "call GetCategory()";
            CallableStatement statement = con.prepareCall(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                int id = result.getInt(1);
                String name = result.getString(2);
                categorias.add(new Categorias(id, name));
             }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
            con.close();
            return categorias;
        }
    }
}

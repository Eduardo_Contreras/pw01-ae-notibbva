/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.dao;

import com.mycompany.notibbva.models.Comentarios;
import com.mycompany.notibbva.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eduar
 */
public class ComentarioDAO {
    
    public static int AddComent(Comentarios com, String Tit, String Est){
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call ModAprNoticia(?, ?, ?, ?)";
            CallableStatement statement = con.prepareCall(sql); 
            statement.setString(1, Tit);
            statement.setString(2, Est);
            statement.setString(3, com.getDescripcion());
            statement.setString(4, com.getUsuario());
            return statement.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;        
    }
    
    public static List<Comentarios> getcomentario(String Tit) throws SQLException{
    List<Comentarios> com = new ArrayList<>();
    Connection con = null;
    try{
        con = DbConnection.getConnection();  
            String sql = "call GetComentario(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, Tit);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String Us = result.getString(1);
                String Desc = result.getString(2);
                com.add(new Comentarios(Us, Desc));
            }
    }catch(SQLException ex){
        System.out.println(ex.getMessage());
        }finally{
        con.close();
        return com;
    }
    
    }
    
}

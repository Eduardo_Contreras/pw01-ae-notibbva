/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.dao;

import com.mycompany.notibbva.models.Noticia;
import com.mycompany.notibbva.utils.DbConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eduar
 */
public class NoticiaDAO {
    
    public static int AddNew(Noticia not, String Us)
    {
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call AgregaNoticia(?, ?, ?, ?, ?, ?, ?, ?)";
            CallableStatement statement = con.prepareCall(sql);  
            statement.setString(1, not.getTitulo());
            statement.setString(2, not.getDescripcionC());
            statement.setString(3, not.getNoticia());
            statement.setInt(4, not.getIdCateg());
            statement.setString(5, not.getImagen1());
            statement.setString(6, not.getImagen2());
            statement.setString(7, not.getVideo());
            statement.setString(8, Us);
            return statement.executeUpdate();
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public static int UpdateNew(Noticia not)
    {
        try{
            Connection con = DbConnection.getConnection();  
            String sql = "call UpdateNoticia(?, ?, ?, ?, ?, ?, ?)";
            CallableStatement statement = con.prepareCall(sql);  
            statement.setString(1, not.getTitulo());
            statement.setString(2, not.getDescripcionC());
            statement.setString(3, not.getNoticia());
            statement.setInt(4, not.getIdCateg());
            statement.setString(5, not.getImagen1());
            statement.setString(6, not.getImagen2());
            statement.setString(7, not.getVideo());
            return statement.executeUpdate();
        } catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public static List<Noticia> getNews() throws SQLException{
        List<Noticia> news = new ArrayList<>();
        Connection con = null;
        try{
            con = DbConnection.getConnection();  
            String sql = "call GetNoticias()";
            CallableStatement statement = con.prepareCall(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String titulo = result.getString(1);
                String descripcionC = result.getString(2);
                String noticia = result.getString(3);
                int idcatego = result.getInt(4);
                String categoria = result.getString(5);
                String imagen1 = result.getString(6);
                String imagen2 = result.getString(7);
                String video = result.getString(8);
                String estado = result.getString(9);
                int idUsu = result.getInt(10);
                String usuario = result.getString(11);
                int reacciones = result.getInt(12);
                news.add(new Noticia(idUsu, idcatego, reacciones, titulo, descripcionC, noticia, imagen1, imagen2, video, estado,usuario, categoria));
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
        con.close();
        return news;
        }
    }
    
    public static List<Noticia> getNewsP() throws SQLException{
        List<Noticia> news = new ArrayList<>();
        Connection con = null;
        try{
            con = DbConnection.getConnection();  
            String sql = "call GetNoticiasP()";
            CallableStatement statement = con.prepareCall(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String titulo = result.getString(1);
                String descripcionC = result.getString(2);
                String noticia = result.getString(3);
                int idcatego = result.getInt(4);
                String categoria = result.getString(5);
                String imagen1 = result.getString(6);
                String imagen2 = result.getString(7);
                String video = result.getString(8);
                String estado = result.getString(9);
                int idUsu = result.getInt(10);
                String usuario = result.getString(11);
                int reacciones = result.getInt(12);
                news.add(new Noticia(idUsu, idcatego, reacciones, titulo, descripcionC, noticia, imagen1, imagen2, video, estado,usuario, categoria));
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
        con.close();
        return news;
        }
    }
    
    public static Noticia getNewsN(String tit) throws SQLException{
        Noticia news = new Noticia();
        Connection con = null;
        try{
            con = DbConnection.getConnection();  
            String sql = "call GetNoticiasN(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, tit);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String titulo = result.getString(1);
                String descripcionC = result.getString(2);
                String noticia = result.getString(3);
                int idcatego = result.getInt(4);
                String categoria = result.getString(5);
                String imagen1 = result.getString(6);
                String imagen2 = result.getString(7);
                String video = result.getString(8);
                String estado = result.getString(9);
                int idUsu = result.getInt(10);
                String usuario = result.getString(11);
                int reacciones = result.getInt(12);
                news = new Noticia(idUsu, idcatego, reacciones, titulo, descripcionC, noticia, imagen1, imagen2, video, estado,usuario, categoria);
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
        con.close();
        return news;
        }
    }
    
    public static List<Noticia> getNewsSearch(String tit) throws SQLException{
        List<Noticia> news = new ArrayList<>();
        Connection con = null;
        try{
            con = DbConnection.getConnection();  
            String sql = "call BuscaNoticia(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, tit);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String titulo = result.getString(1);
                String descripcionC = result.getString(2);
                String noticia = result.getString(3);
                int idcatego = result.getInt(4);
                String categoria = result.getString(5);
                String imagen1 = result.getString(6);
                String imagen2 = result.getString(7);
                String video = result.getString(8);
                String estado = result.getString(9);
                int idUsu = result.getInt(10);
                String usuario = result.getString(11);
                int reacciones = result.getInt(12);
                news.add(new Noticia(idUsu, idcatego, reacciones, titulo, descripcionC, noticia, imagen1, imagen2, video, estado,usuario, categoria)) ;
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
        con.close();
        return news;
        }
    }
    
    public static List<Noticia> getNewsGuardada(String tit) throws SQLException{
        List<Noticia> news = new ArrayList<>();
        Connection con = null;
        try{
            con = DbConnection.getConnection();  
            String sql = "call GetNoticiasG(?)";
            CallableStatement statement = con.prepareCall(sql);
            statement.setString(1, tit);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String titulo = result.getString(1);
                String descripcionC = result.getString(2);
                String noticia = result.getString(3);
                int idcatego = result.getInt(4);
                String categoria = result.getString(5);
                String imagen1 = result.getString(6);
                String imagen2 = result.getString(7);
                String video = result.getString(8);
                String estado = result.getString(9);
                int idUsu = result.getInt(10);
                String usuario = result.getString(11);
                int reacciones = result.getInt(12);
                news.add(new Noticia(idUsu, idcatego, reacciones, titulo, descripcionC, noticia, imagen1, imagen2, video, estado,usuario, categoria)) ;
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
        con.close();
        return news;
        }
    }
    
    public static List<Noticia> getNewsR() throws SQLException{
        List<Noticia> news = new ArrayList<>();
        Connection con = null;
        try{
            con = DbConnection.getConnection();  
            String sql = "call GetNoticiaR()";
            CallableStatement statement = con.prepareCall(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String titulo = result.getString(1);
                String descripcionC = result.getString(2);
                String noticia = result.getString(3);
                int idcatego = result.getInt(4);
                String categoria = result.getString(5);
                String imagen1 = result.getString(6);
                String imagen2 = result.getString(7);
                String video = result.getString(8);
                String estado = result.getString(9);
                int idUsu = result.getInt(10);
                String usuario = result.getString(11);
                int reacciones = result.getInt(12);
                news.add(new Noticia(idUsu, idcatego, reacciones, titulo, descripcionC, noticia, imagen1, imagen2, video, estado,usuario, categoria));
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }finally{
        con.close();
        return news;
        }
    }
}

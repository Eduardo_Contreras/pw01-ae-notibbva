 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.controller;

import com.mycompany.notibbva.dao.NoticiaDAO;
import com.mycompany.notibbva.dao.UserDAO;
import com.mycompany.notibbva.models.Noticia;
import com.mycompany.notibbva.models.User;
import com.mycompany.notibbva.utils.FileUtils;
import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
 
/**
 *
 * @author eduar
 */
@WebServlet(name = "SignInController", urlPatterns = {"/SignInController"})
@MultipartConfig(maxFileSize = 1024*1024*5, maxRequestSize = 1024*1024*25)
public class SignInController extends HttpServlet {
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("UsuarioR");
        String pass = request.getParameter("ContraR");
        String email = request.getParameter("CorreoR");
        Part file = request.getPart("Imagen");
        String nameImage = file.getName() + System.currentTimeMillis() + FileUtils.GetExtension(file.getContentType());
        String path = request.getServletContext().getRealPath("");
        String fullpath = path + FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
        file.write(fullpath);
        String DirImage = FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
        
        User us = new User(user, pass, email, DirImage);
        if(UserDAO.insertUser(us)== 1){
             List<Noticia> noti = null;
            try {
                noti = NoticiaDAO.getNews();
            } catch (SQLException ex) {
                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("nNew", noti);
            request.getRequestDispatcher("Index.jsp").forward(request, response);
        }else {
            response.sendRedirect("Login.jsp");
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}

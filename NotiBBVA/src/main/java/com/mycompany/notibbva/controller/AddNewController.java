/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.controller;

import com.mycompany.notibbva.dao.NoticiaDAO;
import com.mycompany.notibbva.models.Noticia;
import com.mycompany.notibbva.utils.FileUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author eduar
 */
@WebServlet(name = "AddNewController", urlPatterns = {"/AddNewController"})
@MultipartConfig(maxFileSize = 1024*1024*5, maxRequestSize = 1024*1024*25)
public class AddNewController extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String titulo = request.getParameter("Titulo");
        String descripcionC = request.getParameter("DescC");
        String noticia = request.getParameter("DescN");
        int categoria = Integer.parseInt(request.getParameter("Categoria"),10);
        Part file1 = request.getPart("Imagen");
        String nameImage = file1.getName() + System.currentTimeMillis() + FileUtils.GetExtension(file1.getContentType());
        String ext = nameImage.substring(nameImage.lastIndexOf("."));
        String DirImage="";
        if(!".ext".equals(ext)){
        String path = request.getServletContext().getRealPath("");
        String fullpath = path + FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
        file1.write(fullpath);
        DirImage = FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
        }
        else
            DirImage = "";
        
        String DirImage2 = "";
        Part file2 = request.getPart("Imagen2");
        String nameImage2 = file2.getName() + System.currentTimeMillis() + FileUtils.GetExtension(file2.getContentType());
        ext = nameImage2.substring(nameImage2.lastIndexOf("."));
        if(!".ext".equals(ext)){
        String path2 = request.getServletContext().getRealPath("");
        String fullpath2 = path2 + FileUtils.RUTE_USER_IMAGE + "/" + nameImage2;
        file2.write(fullpath2);
        DirImage2 = FileUtils.RUTE_USER_IMAGE + "/" + nameImage2;
        }
        else
            DirImage2 = "";
        
        String DirImage3 = "";
        Part file3 = request.getPart("Video");
        String nameImage3 = file3.getName() + System.currentTimeMillis() + FileUtils.GetExtension(file3.getContentType());
        ext = nameImage3.substring(nameImage3.lastIndexOf("."));
        if(!".ext".equals(ext)){
        String path3 = request.getServletContext().getRealPath("");
        String fullpath3 = path3 + FileUtils.RUTE_USER_IMAGE + "/" + nameImage3;
        file3.write(fullpath3);
        DirImage3 = FileUtils.RUTE_USER_IMAGE + "/" + nameImage3;
        }
        else
            DirImage3 = "";
        
        String Us = request.getParameter("UsuarioE");
        Noticia Nueva = new Noticia(titulo, descripcionC, noticia, categoria, DirImage, DirImage2, DirImage3);
        if(NoticiaDAO.AddNew(Nueva, Us)==1)
        {
            List<Noticia> noti = null;
            try {
                noti = NoticiaDAO.getNews();
            } catch (SQLException ex) {
                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
            request.setAttribute("nNew", noti);
            request.getRequestDispatcher("InAdministrador.jsp").forward(request, response);
            }
        }
            
            
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

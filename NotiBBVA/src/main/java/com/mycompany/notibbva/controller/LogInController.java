/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.controller;

import com.mycompany.notibbva.dao.NoticiaDAO;
import com.mycompany.notibbva.dao.UserDAO;
import com.mycompany.notibbva.models.Noticia;
import com.mycompany.notibbva.models.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author eduar
 */
@WebServlet(name = "LogInController", urlPatterns = {"/LogInController"})
public class LogInController extends HttpServlet {
 

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("UsuarioL");
        String pass = request.getParameter("ContraL");
        User usLog = new User(user, pass);
            User result = UserDAO.LogInUser(usLog);
        if(result != null)
        {
            HttpSession session = request.getSession();
            session.setAttribute("Usuario", result.getUser());
            session.setAttribute("Rol", result.getRol());
            session.setAttribute("Imagen", result.getPathimage());
            String rol = result.getRol();
            List<Noticia> noti = null;
            try {
                noti = NoticiaDAO.getNews();
            } catch (SQLException ex) {
                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("nNew", noti);
            request.getRequestDispatcher("InAdministrador.jsp").forward(request, response);
        }else
        response.sendRedirect("EditPerfil.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}

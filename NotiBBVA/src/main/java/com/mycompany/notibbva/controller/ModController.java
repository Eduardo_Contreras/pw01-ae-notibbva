/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.controller;

import com.mycompany.notibbva.dao.UserDAO;
import com.mycompany.notibbva.models.User;
import com.mycompany.notibbva.utils.FileUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author eduar
 */
@WebServlet(name = "ModController", urlPatterns = {"/ModController"})
@MultipartConfig(maxFileSize = 1024*1024*5, maxRequestSize = 1024*1024*25)
public class ModController extends HttpServlet {
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String Us = request.getParameter("UsuarioM");
        String pass = request.getParameter("ContraM");
        String email = request.getParameter("CorreoM");
        String Mod = "";
        Part file = request.getPart("ImagenM");
        String nameImage = file.getName() + System.currentTimeMillis() + FileUtils.GetExtension(file.getContentType());
         String ext = nameImage.substring(nameImage.lastIndexOf("."));
        if(!"".equals(pass))
        {
            Mod = "P";
        }
        if(!"".equals(email))
        {
            Mod = Mod.concat("E");
        }
        if(!".ext".equals(ext))
        {
            Mod = Mod.concat("I");
            String path = request.getServletContext().getRealPath("");
            String fullpath = path + FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
            file.write(fullpath);
        }
        String DirImage = FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
       
        
        User usmod = new User(Us, pass, email, DirImage);
        if(UserDAO.ModUser(usmod,Mod)== 1){
            HttpSession session = request.getSession();
            session.invalidate();
            response.sendRedirect("Login.jsp");
        }else {
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}

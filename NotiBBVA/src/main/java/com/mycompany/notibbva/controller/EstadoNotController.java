/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.controller;

import com.mycompany.notibbva.dao.ComentarioDAO;
import com.mycompany.notibbva.dao.NoticiaDAO;
import com.mycompany.notibbva.models.Comentarios;
import com.mycompany.notibbva.models.Noticia;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author eduar
 */
@WebServlet(name = "EstadoNotController", urlPatterns = {"/EstadoNotController"})
public class EstadoNotController extends HttpServlet {


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String titulo = request.getParameter("Titulo");
        String UsCom = request.getParameter("UsuarioC");
        int Est = Integer.parseInt(request.getParameter("Estado"),10);
        String Estado = "";
        if(Est == 0)
            Estado = "No Aprobado";
        else if(Est == 1)
            Estado = "Aprobado";
        String Comentario = request.getParameter("motivo");
        Comentarios com = new Comentarios(UsCom, Comentario);
        if(ComentarioDAO.AddComent(com, titulo, Estado) == 1)
        {
            List<Noticia> noti = null;
            try {
                noti = NoticiaDAO.getNews();
            } catch (SQLException ex) {
                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
            request.setAttribute("nNew", noti);
            request.getRequestDispatcher("InAdministrador.jsp").forward(request, response);
            }
        }
            
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.controller;

import com.mycompany.notibbva.dao.NoticiaDAO;
import com.mycompany.notibbva.dao.UserDAO;
import com.mycompany.notibbva.models.Noticia;
import com.mycompany.notibbva.models.User;
import com.mycompany.notibbva.utils.FileUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author eduar
 */
@WebServlet(name = "SignInAdmController", urlPatterns = {"/SignInAdmController"})
@MultipartConfig(maxFileSize = 1024*1024*5, maxRequestSize = 1024*1024*25)
public class SignInAdmController extends HttpServlet {



    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("UsuarioA");
        String pass = request.getParameter("ContraA");
        String email = request.getParameter("CorreoA");
        String rol = request.getParameter("RolA");
        Part file = request.getPart("Imagen");
        String nameImage = file.getName() + System.currentTimeMillis() + FileUtils.GetExtension(file.getContentType());
        String path = request.getServletContext().getRealPath("");
        String fullpath = path + FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
        file.write(fullpath);
        String DirImage = FileUtils.RUTE_USER_IMAGE + "/" + nameImage;
        
        User usu = new User(user, pass, email, rol, DirImage);
        if(UserDAO.LogInAdminUser(usu)== 1){
            List<Noticia> noti = null;
            try {
                noti = NoticiaDAO.getNews();
            } catch (SQLException ex) {
                Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
            request.setAttribute("nNew", noti);
            request.getRequestDispatcher("InAdministrador.jsp").forward(request, response);
            }
        }else {
            response.sendRedirect("Login.jsp");
        } 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.models;

/**
 *
 * @author eduar
 */
public class Comentarios {
    private int Id;
    private int IdUs;
    private int IdNot;
    private int Reacciones;
    private String Usuario;
    private String Descripcion;

    public Comentarios() {
    }

    public Comentarios(int Id, int IdUs, int IdNot, int Reacciones, String Usuario, String Descripcion) {
        this.Id = Id;
        this.IdUs = IdUs;
        this.IdNot = IdNot;
        this.Reacciones = Reacciones;
        this.Usuario = Usuario;
        this.Descripcion = Descripcion;
    }

    public Comentarios(String Usuario, String Descripcion) {
        this.Usuario = Usuario;
        this.Descripcion = Descripcion;
    }

    
    
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getIdUs() {
        return IdUs;
    }

    public void setIdUs(int IdUs) {
        this.IdUs = IdUs;
    }

    public int getIdNot() {
        return IdNot;
    }

    public void setIdNot(int IdNot) {
        this.IdNot = IdNot;
    }

    public int getReacciones() {
        return Reacciones;
    }

    public void setReacciones(int Reacciones) {
        this.Reacciones = Reacciones;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }
    
    
    
    
}

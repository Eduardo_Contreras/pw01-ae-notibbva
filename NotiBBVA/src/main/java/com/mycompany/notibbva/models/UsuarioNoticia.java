/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.models;

/**
 *
 * @author eduar
 */
public class UsuarioNoticia {
    private int Usuario;
    private int Noticia;
    private String UsName;
    private String NotName;

    public UsuarioNoticia(int Usuario, int Noticia) {
        this.Usuario = Usuario;
        this.Noticia = Noticia;
    }

    public UsuarioNoticia(String UsName, String NotName) {
        this.UsName = UsName;
        this.NotName = NotName;
    }

    public String getUsName() {
        return UsName;
    }

    public void setUsName(String UsName) {
        this.UsName = UsName;
    }

    public String getNotName() {
        return NotName;
    }

    public void setNotName(String NotName) {
        this.NotName = NotName;
    }
    

    public UsuarioNoticia() {
    }

    public int getUsuario() {
        return Usuario;
    }

    public void setUsuario(int Usuario) {
        this.Usuario = Usuario;
    }

    public int getNoticia() {
        return Noticia;
    }

    public void setNoticia(int Noticia) {
        this.Noticia = Noticia;
    }
    
    
}

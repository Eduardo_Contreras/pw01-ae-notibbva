/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.models;

/**
 * Modelo de Usuario 
 * @author eduar
 */
public class User {
    
    private int Id;
    private String user;
    private String pass;
    private String email;
    private String rol;
    private String pathimage;

    public User() {
    }
      public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
    
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getPathimage() {
        return pathimage;
    }

    public void setPathimage(String pathimage) {
        this.pathimage = pathimage;
    }

    public User(int Id, String user, String pass, String email, String rol, String pathimage) {
        this.Id = Id;
        this.user = user;
        this.pass = pass;
        this.email = email;
        this.rol = rol;
        this.pathimage = pathimage;
    }

    
    public User(int Id, String user) {
        this.Id = Id;
        this.user = user;
    }

    public User(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public User(String user, String rol, String pathimage) {
        this.user = user;
        this.rol = rol;
        this.pathimage = pathimage;
    }

  
    public User(int Id, String user, String pass, String email) {
        this.Id = Id;
        this.user = user;
        this.pass = pass;
        this.email = email;
    }

    public User(String user, String pass, String email, String rol, String pathimage) {
        this.user = user;
        this.pass = pass;
        this.email = email;
        this.rol = rol;
        this.pathimage = pathimage;
    }
    
    public User(String user, String pass, String email, String pathimage) {
        this.user = user;
        this.pass = pass;
        this.email = email;
        this.pathimage = pathimage;
    }

}

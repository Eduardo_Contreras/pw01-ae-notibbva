/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.notibbva.models;

/**
 *
 * @author eduar
 */
public class Noticia {
    private int id;
    private int idUs;
    private int idCateg;
    private int Reacciones;
    private String Titulo;
    private String DescripcionC;
    private String Noticia;
    private String Imagen1;
    private String Imagen2;
    private String Video;
    private String Estado;
    private String NameUs;
    private String NameCate;

    public Noticia() {
    }

    public Noticia(int id, int idUs, int idCateg, int Reacciones, String Titulo, String DescripcionC, String Noticia, String Imagen1, String Imagen2, String Video, String Estado) {
        this.id = id;
        this.idUs = idUs;
        this.idCateg = idCateg;
        this.Reacciones = Reacciones;
        this.Titulo = Titulo;
        this.DescripcionC = DescripcionC;
        this.Noticia = Noticia;
        this.Imagen1 = Imagen1;
        this.Imagen2 = Imagen2;
        this.Video = Video;
        this.Estado = Estado;
    }

    public Noticia(int idUs, int idCateg, int Reacciones, String Titulo, String DescripcionC, String Noticia, String Imagen1, String Imagen2, String Video, String Estado, String NameUs, String NameCate) {
        this.idUs = idUs;
        this.idCateg = idCateg;
        this.Reacciones = Reacciones;
        this.Titulo = Titulo;
        this.DescripcionC = DescripcionC;
        this.Noticia = Noticia;
        this.Imagen1 = Imagen1;
        this.Imagen2 = Imagen2;
        this.Video = Video;
        this.Estado = Estado;
        this.NameUs = NameUs;
        this.NameCate = NameCate;
    }
    

    public Noticia(String Titulo, String DescripcionC, String Noticia, int idCateg, String Imagen1, String Imagen2, String Video) {
        this.idCateg = idCateg;
        this.Titulo = Titulo;
        this.DescripcionC = DescripcionC;
        this.Noticia = Noticia;
        this.Imagen1 = Imagen1;
        this.Imagen2 = Imagen2;
        this.Video = Video;
    }

    public String getNameUs() {
        return NameUs;
    }

    public void setNameUs(String NameUs) {
        this.NameUs = NameUs;
    }

    public String getNameCate() {
        return NameCate;
    }

    public void setNameCate(String NameCate) {
        this.NameCate = NameCate;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUs() {
        return idUs;
    }

    public void setIdUs(int idUs) {
        this.idUs = idUs;
    }

    public int getIdCateg() {
        return idCateg;
    }

    public void setIdCateg(int idCateg) {
        this.idCateg = idCateg;
    }

    public int getReacciones() {
        return Reacciones;
    }

    public void setReacciones(int Reacciones) {
        this.Reacciones = Reacciones;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    public String getDescripcionC() {
        return DescripcionC;
    }

    public void setDescripcionC(String DescripcionC) {
        this.DescripcionC = DescripcionC;
    }

    public String getNoticia() {
        return Noticia;
    }

    public void setNoticia(String Noticia) {
        this.Noticia = Noticia;
    }

    public String getImagen1() {
        return Imagen1;
    }

    public void setImagen1(String Imagen1) {
        this.Imagen1 = Imagen1;
    }

    public String getImagen2() {
        return Imagen2;
    }

    public void setImagen2(String Imagen2) {
        this.Imagen2 = Imagen2;
    }

    public String getVideo() {
        return Video;
    }

    public void setVideo(String Video) {
        this.Video = Video;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }
    
    
}
